import React from "react";
import useFetchCocktails from "../hooks/useFetchCocktails";
import AppItem from "../components/AppItem";
import NotFoundPage from "./NotFoundPage";
import { useCartContext } from "../components/CartContext";

const MainPage = () => {
    const apiRandomCocktail = 'https://www.thecocktaildb.com/api/json/v1/1/random.php';
    const { data, isLoaded, error } = useFetchCocktails(apiRandomCocktail);
    const { confirmOrders } = useCartContext();
    console.log(confirmOrders);

    if(error) {
        return <p className="error">Error {error.message}</p>
    } else if (!isLoaded) {
        return (
            <div className = "container_loader">
                <span className = "loader">Loading... </span>
            </div>
        );
    } else if (data === null) {
        <NotFoundPage />
    } else {
        return (
            <div className="first_container">
                <div className="second_container">
                    <h1>To choose a cocktail, use the search or filter</h1>
                    <div className="line"></div>
                    <h2 className="orange">Personal recommendation</h2>
                    <div className="content">
                        {data.map(item => (
                            <AppItem 
                                key = {item.idDrink}
                                id = {item.idDrink}
                                title = {item.strDrink}
                                img = {item.strDrinkThumb}
                                instructions = {item.strInstructions}
                                ingredients = {[
                                    item.strIngredient1,
                                    item.strIngredient2,
                                    item.strIngredient3,
                                    item.strIngredient4,
                                    item.strIngredient5,
                                    item.strIngredient6,
                                    item.strIngredient7,
                                    item.strIngredient8,
                                    item.strIngredient9,
                                    item.strIngredient10,
                                    item.strIngredient11,
                                    item.strIngredient12,
                                    item.strIngredient13,
                                    item.strIngredient14,
                                    item.strIngredient15
                                ]}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

export default MainPage;