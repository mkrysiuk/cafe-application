import React from "react";
import { NavLink } from "react-router-dom";

const NotFoundPage = () => {
    return (
        <div className="content">
            <h3>No drinks found! Go <NavLink to="/">Main Page</NavLink></h3>
        </div>
    )
}

export default NotFoundPage;