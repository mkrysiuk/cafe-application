import React from "react";
import useFetchCocktails from "../hooks/useFetchCocktails";
import { useParams } from "react-router-dom";
import NotFoundPage from "./NotFoundPage";
import AppItemList from "../components/AppItemList";

const ListCocktailsBySearch = () => {
    const { search } = useParams();
    const apiSearchCocktailByFieldSearch = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${search}`;
    const { data, isLoaded, error } = useFetchCocktails(apiSearchCocktailByFieldSearch);

    if(error) {
        return <p className="error">Error {error.message}</p>
    } else if (!isLoaded) {
        return (
            <div className = "container_loader">
                <span className = "loader">Loading... </span>
            </div>
        );
    } else if (data === null) {
        return (
            <NotFoundPage />
        );
    } else {
        return (
            <div className="first_container">
                <div className="second_container">
                    <ul className="render_list">
                        {data.map(item => (
                            <AppItemList 
                                key = {item.idDrink}
                                id = {item.idDrink}
                                title = {item.strDrink}
                                img = {item.strDrinkThumb}
                                search = {search}
                            />
                        ))}
                    </ul>
                </div>
            </div>
        );
    }
}

export default ListCocktailsBySearch;