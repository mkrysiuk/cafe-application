import React, { useState } from "react";
import { NavLink, Outlet } from "react-router-dom";
import { useCartContext } from "./CartContext";

import { iconShoppingCart } from "../style/img/img_svg";

const Layout = () => {
    const { orders, changeIsOpen, showAlertMsg } = useCartContext();
    const [ inputText, setInputText ] = useState("");
    const [ burger_menu, setBurgerMenu ] = useState("burger_menu hidden");

    const clearInput = () => setInputText("");

    const showBurger = () => {
        if(burger_menu === "burger_menu hidden") {
            changeIsOpen(true);
            setBurgerMenu("burger_menu visible");
        } else {
            changeIsOpen(false);
            setBurgerMenu("burger_menu hidden");
        }
    }

    return (
        <div className="wrapper">
            <div className="header_container">
                <header className="header">
                    <div className="nav_bar">
                    <NavLink to="/"><h3>Cocktails by Nicolas</h3></NavLink>
                        <nav className={burger_menu === "burger_menu hidden" ? "nav-link" : burger_menu}>
                            <NavLink to="/">Main Page</NavLink>
                            <form>
                                <input
                                    type="text"
                                    value={inputText}
                                    placeholder="Search"
                                    onChange={(evt) => setInputText(evt.target.value)}
                                    className="search"
                                />
                                <NavLink to={`/list-cocktails-search/${inputText}`}>
                                    <button
                                        disabled={inputText === ""}
                                        type="submit"
                                        onClick={clearInput}
                                        className="btn_search"
                                    >
                                        Search
                                    </button>
                                </NavLink>
                            </form>
                            <NavLink 
                                to={orders.length !== 0 ? "shopping-cart" : "/"}
                                onClick={orders.length !== 0 ? () => changeIsOpen(true) : () => showAlertMsg()}
                            >
                                {iconShoppingCart}
                                <span className={orders.length !== 0 ? "cart_count" : ""}>
                                    {orders.length !== 0 ? orders.length : ""}
                                </span>
                            </NavLink>
                        </nav>
                        <button className="burger" onClick={() => showBurger()}>
                            { burger_menu === "burger_menu hidden"
                            ?
                                <div className="burger-btn"></div>
                            :
                                <div className="btn_close_burger">+</div>
                            }
                        </button>
                    </div>
                </header>
            </div>

            <main className="main_container">
                <Outlet />
            </main>

            <footer className="main_footer_container">
                <div className="footer_container">
                    <div className="footer_content">
                    <h2>Filter cocktails by the first letter:</h2>
                        <h3>
                            <NavLink to="/list-cocktails/A"> A <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/B"> B <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/C"> C <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/D"> D <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/E"> E <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/F"> F <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/G"> G <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/H"> H <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/I"> I <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/J"> J <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/K"> K <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/L"> L <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/M"> M <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/N"> N <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/O"> O <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/P"> P <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/Q"> Q <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/R"> R <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/S"> S <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/T"> T <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/U"> U <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/V"> V <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/W"> W <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/X"> X <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/Y"> Y <span>/</span></NavLink>
                            <NavLink to="/list-cocktails/Z"> Z </NavLink>
                        </h3>
                        <div className="line"></div>
                        <span>&copy; by mkrysiuk 2022</span>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default Layout;