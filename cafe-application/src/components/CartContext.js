import React, { useState, useContext } from "react";

const CartContext = React.createContext();

export const useCartContext = () => {
    return useContext(CartContext);
}

const CartProvider = ({ children }) => {
    const [ isOpen, setIsOpen ] = useState(false);
    const [ orders, setOrders ] = useState([]);
    const [ idOrder, setIdOrder ] = useState(0);
    const [ confirmOrders, setConfirmOrders ] = useState([]);

    const changeIsOpen = (value) => setIsOpen(value);
    const showAlertMsg = () => alert("First, choose a cocktail!");
    const removeOrders = (id) => setOrders(orders.filter((order) => order.id !== id));

    const addOrdersToCart = (id, title, img, ingredients, instructions) => {
        const newOrders = {
            id: id,
            title: title,
            img: img,
            ingredients: ingredients,
            instructions: instructions
        }
        setOrders([...orders, newOrders]);
        setIdOrder(id);
    }

    const addConfirmOrders = (confirmOrders) => {
        setConfirmOrders(confirmOrders);
        setOrders("");
        setIsOpen(false);
        alert("Order accepted");
    }

    return (
        <CartContext.Provider value={{
            open: isOpen,
            orders: orders,
            confirmOrders: confirmOrders,
            idOrder: idOrder,
            changeIsOpen,
            addOrdersToCart,
            addConfirmOrders,
            removeOrders,
            showAlertMsg
        }}>
            { children }
        </CartContext.Provider>
    );
}

export default CartProvider;