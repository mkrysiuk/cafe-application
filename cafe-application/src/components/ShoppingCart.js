import React from 'react';
import ReactDOM from 'react-dom';
import { NavLink } from "react-router-dom";
import { useCartContext } from './CartContext';

const ShoppingCart = () => {
    const { open, orders, changeIsOpen, addConfirmOrders, removeOrders } = useCartContext();
    
    return (    
        open && ReactDOM.createPortal(
            <div className="wrapper">
                <div className="modal_container">
                    <div className="modal_inner">
                        <header className="modal_header">
                            <h3>Shopping Cart</h3>
                            <NavLink to="/">
                                <button className="btn_close_modal" onClick={() => changeIsOpen(false)}>+</button>
                            </NavLink>
                        </header>
                        <main className="modal_main">
                            <h1>Orders</h1>
                            {orders.length === 0 ? <h4>The cart is empty</h4> : orders.map((order) => (
                                <div key={order.id} className="modal_order">
                                    <img className="img_cocktails" src={order.img} alt={order.title} />
                                    <h3>{order.title}</h3>
                                    <button className="btn_remove" onClick={() => removeOrders(order.id)}>Remove</button>
                                </div>
                            ))}
                        </main>
                        <footer className="modal_footer">
                            <NavLink to="/">
                                <button
                                    disabled={orders.length === 0}
                                    onClick={() => addConfirmOrders(orders)}
                                    className="btn"
                                    >
                                        Confirm
                                </button>
                            </NavLink>
                        </footer>
                    </div>
                </div>
            </div>,
        document.getElementById('modal'))
    );
}

export default ShoppingCart;