import React from "react";
import { Routes, Route } from "react-router-dom";

import MainPage from "../Pages/MainPage";
import ListCocktailsByFilter from "../Pages/ListCocktailByFilter";
import ListCocktailsBySearch from "../Pages/ListCocktailsBySearch";
import SinglePage from "../Pages/SinglePage";
import NotFoundPage from "../Pages/NotFoundPage";

import ShoppingCart from "./ShoppingCart";
import Layout from "./Layout";
import CartProvider from "./CartContext";


const App = () => {
    return (
        <CartProvider>
            <Routes>
                <Route path="/" element={<Layout /> }>
                    <Route index element={<MainPage />} />
                    <Route path="shopping-cart" element = {<ShoppingCart />} />
                    <Route path="list-cocktails/:letter" element={<ListCocktailsByFilter />} />
                    <Route path="list-cocktails/:letter/:id" element={<SinglePage />} />
                    <Route path="list-cocktails-search/:search" element={<ListCocktailsBySearch />} />
                    <Route path="list-cocktails-search/:search/:id" element={<SinglePage />} />
                    <Route path="*" element={<NotFoundPage />} />
                </Route>
            </Routes>
        </CartProvider>
    );
}

export default App;