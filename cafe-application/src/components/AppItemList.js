import React from "react";
import { NavLink } from "react-router-dom";

const AppItemList = (props) => {
    const { id, title, img, letter, search } = props;

    return (
        <li key = {id} className="list">
            <NavLink  to={`/list-cocktails/${letter ? letter : search}/${id}`}>
                <h4>{title}</h4>
                <img className="img_cocktails" src={img} alt={title} />
                <button className="btn">more information</button>
            </NavLink>   
        </li>
    );
}

export default AppItemList;