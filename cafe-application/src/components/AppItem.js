import React from "react";
import { useCartContext } from "./CartContext";

const AppItem = (props) => {
    const { addOrdersToCart, idOrder } = useCartContext();

    const {
        id,
        title,
        img,
        instructions,
        ingredients
    } = props;

    const ingredientsFilter = ingredients ? ingredients.filter(item => item !== null && item !== undefined) : [];

    return (
        <>
            <div className="list_inner">
                <div className="list_inner-left">
                    <h3>{title}</h3>
                    <img className="img_cocktails" src={img} alt={title} />
                </div>
                <div className="list_inner-right">
                    <h3>Ingredients:</h3>
                    <div className="list_ingredients">
                        {ingredientsFilter.length !== 0 
                            ? ingredientsFilter.map((item, index) => (
                                <span key={index}>{item};</span>
                            )) 
                            : <span>No data on the ingredients!</span>
                        }
                    </div>
                    <div className="line"></div>
                    <h3>Instructions:</h3>
                    <p>{instructions}</p>
                </div>
            </div>
            <button
                type="button"
                onClick={() => addOrdersToCart(id, title, img, ingredients, instructions)}
                className="btn"
                disabled = {idOrder === id}
            >
                {idOrder !== id ? "add to cart" : "In the cart" }
            </button>
        </>
    );
}

export default AppItem;