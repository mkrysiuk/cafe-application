import { useState, useEffect } from "react";

const useFetchCocktails = (apiRandomCocktail) => {
    const [ isLoaded, setLoaded ] = useState(false);
    const [ error, setError ] = useState(null);
    const [ data, setData ] = useState([]);
    
    const fetchCocktails = (apiRandomCocktail) => {
        fetch(apiRandomCocktail) 
            .then((response) => response.json())
            .then((result) => {
                    setLoaded(true);
                    setData(result.drinks);
            })
            .catch((err) => {
                    setLoaded(true);
                    setError(err);
            })
    }

    useEffect(() => {
        if(apiRandomCocktail) {
            fetchCocktails(apiRandomCocktail);
        }
    }, [apiRandomCocktail]);
    
    return { isLoaded, error, data };
}

export default useFetchCocktails;